package com.nabeel.cameraapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.File
import java.io.Serializable

@Entity(tableName = "ImageFileTable")
data class ImageFileEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long,
    val size: String,
    val path: String,
    val name: String,
    val date: String,
    val lat: Double,
    val lon: Double
) : Serializable
