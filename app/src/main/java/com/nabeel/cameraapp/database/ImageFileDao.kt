package com.nabeel.cameraapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ImageFileDao {
    @Query("SELECT * from ImageFileTable ORDER BY id DESC")
    fun getAllImageFiles(): LiveData<List<ImageFileEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addImageFileToDb(imageFileEntity: ImageFileEntity)
}