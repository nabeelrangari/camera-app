package com.nabeel.cameraapp.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.appbar.MaterialToolbar

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    protected var mViewModel: VM? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        prepareBeforeInitView()
        setToolbar()
        initVM()
    }

    private fun setToolbar() {
        providerToolBar()?.let { setSupportActionBar(it) }
    }

    /**
     * id
     */
    abstract fun layoutId(): Int

    open fun prepareBeforeInitView() {}
    private fun initVM() {
        providerVMClass()?.let { it ->
            mViewModel = ViewModelProvider(this).get(it)
            lifecycle.addObserver(mViewModel!!)
        }
    }

    /**
    [Toolbar]
     */
    open fun providerToolBar(): MaterialToolbar? = null

    /**
     * [BaseViewModel]
     */
    open fun providerVMClass(): Class<VM>? = null

    override fun onDestroy() {
        mViewModel?.let {
            lifecycle.removeObserver(it)
        }

        super.onDestroy()
    }
}