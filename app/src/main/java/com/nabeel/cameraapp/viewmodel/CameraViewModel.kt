package com.nabeel.cameraapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.nabeel.cameraapp.base.BaseViewModel
import com.nabeel.cameraapp.database.ImageFileEntity
import com.nabeel.cameraapp.utils.repository
import kotlinx.coroutines.Dispatchers

class CameraViewModel : BaseViewModel() {

    fun getAllImageFiles(): LiveData<List<ImageFileEntity>> = liveData(Dispatchers.IO) {
        try {
            val imageFiles = repository.getAllImageFiles()
            emitSource(imageFiles)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun addImageFile(imageFileEntity: ImageFileEntity): LiveData<List<ImageFileEntity>> =
        liveData(Dispatchers.IO) {
            try {
                val imageFile = repository.addImageFile(imageFileEntity)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
}