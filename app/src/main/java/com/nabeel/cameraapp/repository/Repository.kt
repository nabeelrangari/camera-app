package com.nabeel.cameraapp.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.nabeel.cameraapp.database.AppDatabase
import com.nabeel.cameraapp.database.ImageFileEntity

class Repository constructor(context: Context) {
    private val database: AppDatabase = AppDatabase.getDatabse(context)
    private var imageFile = database.imageFileDao()

    fun getAllImageFiles(): LiveData<List<ImageFileEntity>> = imageFile.getAllImageFiles()

    fun addImageFile(imageFileEntity: ImageFileEntity) = imageFile.addImageFileToDb(imageFileEntity)


}