package com.nabeel.cameraapp.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.nabeel.cameraapp.R
import com.nabeel.cameraapp.base.BaseActivity
import com.nabeel.cameraapp.database.ImageFileEntity
import com.nabeel.cameraapp.utils.RecyclerItemClickListener
import com.nabeel.cameraapp.view.adapter.ImageFileAdapter
import com.nabeel.cameraapp.viewmodel.CameraViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<CameraViewModel>() {

    private lateinit var ll_sort: LinearLayout
    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var cl_no_data: ConstraintLayout
    private lateinit var fbt_camera: FloatingActionButton
    private var imageFileList: List<ImageFileEntity> = ArrayList()
    private lateinit var adapter: ImageFileAdapter

    override fun providerVMClass(): Class<CameraViewModel> = CameraViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ), 3
                )
        }

        ll_sort = findViewById(R.id.ll_sort)
        cl_no_data = findViewById(R.id.cl_no_data)
        recyclerView = findViewById(R.id.rv_photos)
        fbt_camera = findViewById(R.id.fbt_camera)
        layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                recyclerView,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        startActivity(
                            Intent(this@MainActivity, DetailsActivity::class.java)
                                .putExtra("imageDetails", imageFileList[position])
                        )
                    }
                })
        )

        fbt_camera.setOnClickListener {
            startActivity(Intent(this@MainActivity, CameraActivity::class.java))
        }

        rbt_sort_name.setOnClickListener {
            rbt_sort_name.isChecked = true
            rbt_sort_size.isChecked = false
            imageFileList = sortByName()
            adapter.setData(imageFileList)
        }

        rbt_sort_size.setOnClickListener {
            rbt_sort_name.isChecked = false
            rbt_sort_size.isChecked = true
            imageFileList = sortBySize()
            adapter.setData(imageFileList)
        }
        setData()
    }

    private fun setData() {
        adapter = ImageFileAdapter(imageFileList, this)
        rv_photos.adapter = adapter

        mViewModel?.getAllImageFiles()?.observe(this, Observer {
            it.let {
                imageFileList = it
                if (imageFileList.isEmpty()) {
                    ll_sort.visibility = View.GONE
                    cl_no_data.visibility = View.VISIBLE
                    rv_photos.visibility = View.GONE
                } else {
                    ll_sort.visibility = View.VISIBLE
                    rv_photos.visibility = View.VISIBLE
                    cl_no_data.visibility = View.GONE
                }
                adapter.setData(imageFileList)
            }
        })
    }

    private fun sortByName(): List<ImageFileEntity> {
        return imageFileList.sortedBy { it.name }
    }

    private fun sortBySize(): List<ImageFileEntity> {
        return imageFileList.sortedBy { it.size }
    }

    override fun layoutId(): Int = R.layout.activity_main
}
