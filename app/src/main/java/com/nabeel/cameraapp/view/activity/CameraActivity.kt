package com.nabeel.cameraapp.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.google.android.cameraview.CameraImpl
import com.google.android.gms.location.*
import com.nabeel.cameraapp.R
import com.nabeel.cameraapp.base.BaseActivity
import com.nabeel.cameraapp.database.ImageFileEntity
import com.nabeel.cameraapp.utils.Converters
import com.nabeel.cameraapp.viewmodel.CameraViewModel
import io.reactivex.disposables.Disposable
import me.pqpo.smartcameralib.MaskView
import me.pqpo.smartcameralib.SmartCameraView
import me.pqpo.smartcameralib.SmartScanner
import java.io.File

class CameraActivity : BaseActivity<CameraViewModel>() {
    private lateinit var mCameraView: SmartCameraView
    private lateinit var ic_camera: AppCompatImageView
    private lateinit var ic_gallery: AppCompatImageView
    private val granted = true
    private var dialog: Dialog? = null
    private var disposable: Disposable? = null

    val PERMISSION_ID = 100
    var mLocation: Location? = null
    lateinit var mFusedLocationClient: FusedLocationProviderClient

    companion object {
        //image pick code
        private val IMAGE_PICK_CODE = 1000;
        //Permission code
        private val PERMISSION_CODE = 1001;
    }

    override fun providerVMClass(): Class<CameraViewModel> = CameraViewModel::class.java
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mCameraView = findViewById(R.id.camera_view)
        ic_camera = findViewById(R.id.ic_camera)
        ic_gallery = findViewById(R.id.ic_gallery)
        initMaskView()
        initScannerParams()
        initCameraView()
        OnClick()
        getLastLocation()
    }

    private fun OnClick() {
        ic_camera.setOnClickListener {
            mCameraView.takePicture()
            mCameraView.stopScan()
        }

        ic_gallery.setOnClickListener {
            //check runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_DENIED
                ) {
                    //permission denied
                    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE);
                } else {
                    //permission already granted
                    pickImageFromGallery();
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data?.data)
            showPicture(bitmap)
        }
    }

    private fun initCameraView() {
        //mCameraView.getSmartScanner().setPreview(true);
        mCameraView.setOnScanResultListener { smartCameraView, result, yuvData ->
            val previewBitmap = smartCameraView.previewBitmap
            if (previewBitmap != null) {
                //ivPreview.setImageBitmap(previewBitmap);
            }
            false
        }
        mCameraView.addCallback(object : CameraImpl.Callback() {
            override fun onPictureTaken(
                camera: CameraImpl,
                data: ByteArray
            ) {
                super.onPictureTaken(camera, data)
                mCameraView.cropJpegImage(data) { cropBitmap -> cropBitmap?.let { showPicture(it) } }
            }
        }
        )
    }

    private fun showPicture(bitmap: Bitmap) {
        dialog = Dialog(this)
        dialog?.setContentView(R.layout.dialog_image_view)
        dialog?.setCancelable(true)
        val window = dialog?.window
        window?.setBackgroundDrawableResource(R.color.colorTrans)
        val ic_image: AppCompatImageView = dialog!!.findViewById(R.id.ic_image)
        val bt_retake: AppCompatButton = dialog!!.findViewById(R.id.bt_retake)
        val bt_save: AppCompatButton = dialog!!.findViewById(R.id.bt_save)
        ic_image.setImageBitmap(bitmap)
        bt_retake.setOnClickListener {
            dialog?.dismiss()
            mCameraView.startScan()
        }
        bt_save.setOnClickListener {
            disposable = Converters.convertBitmapToFile(bitmap) { file ->
                setImageFileData(file)
            }
            dialog?.dismiss()
            finish()
        }
        dialog?.show()
    }

    private fun setImageFileData(file: File) {
        val imageFileData = ImageFileEntity(
            0,
            "${file.length() / 1024}  KB",
            file.path,
            file.name,
            Converters.getFileDate(file.lastModified()),
            mLocation?.latitude!!,
            mLocation?.longitude!!
        )

        mViewModel?.addImageFile(imageFileData)?.observe(this, Observer {
            it.let {
                Toast.makeText(this, "Saved Picture Path ${imageFileData.name}", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    private fun initMaskView() {
        val maskView = mCameraView.maskView as MaskView
        maskView.setMaskLineColor(-0xff524b)
        maskView.setShowScanLine(false)
        maskView.setScanLineGradient(-0xff524b, 0x0000adb5)
        maskView.setMaskLineWidth(2)
        maskView.setMaskRadius(5)
        maskView.setScanSpeed(6)
        maskView.setScanGradientSpread(80)
        mCameraView.post {
            val width = mCameraView.width
            val height = mCameraView.height
            if (width < height) {
                maskView.setMaskSize((width * 0.9f).toInt(), (width * 0.4f / 0.63).toInt())
                maskView.setMaskOffset(0, (-(width * 0.1)).toInt())
            } else {
                maskView.setMaskSize((width * 0.9f).toInt(), (width * 0.4f * 0.63).toInt())
            }
        }
        mCameraView.maskView = maskView
    }

    private fun initScannerParams() {
        SmartScanner.DEBUG = true
        SmartScanner.cannyThreshold1 = 20
        SmartScanner.cannyThreshold2 = 50
        SmartScanner.houghLinesThreshold = 130
        SmartScanner.houghLinesMinLineLength = 80
        SmartScanner.houghLinesMaxLineGap = 10
        SmartScanner.gaussianBlurRadius = 3
        SmartScanner.detectionRatio = 0.1f
        SmartScanner.checkMinLengthRatio = 0.8f
        SmartScanner.maxSize = 300f
        SmartScanner.angleThreshold = 5f
        // don't forget reload params
        SmartScanner.reloadParams()
    }

    override fun onResume() {
        super.onResume()
        // request Camera permission first!
        if (granted) {
            mCameraView.start()
            mCameraView.startScan()
        }
    }

    override fun onPause() {
        mCameraView.stop()
        super.onPause()
        if (dialog != null && dialog!!.isShowing) {
            dialog?.dismiss()
        }
        mCameraView.stopScan()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (dialog != null && dialog!!.isShowing) {
            dialog?.dismiss()
        }
    }

    override fun onDestroy() {
        if (disposable != null)
            disposable!!.dispose()
        super.onDestroy()
    }

    override fun layoutId(): Int = R.layout.activity_camera

    override fun prepareBeforeInitView() {
        super.prepareBeforeInitView()

    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        mLocation = location
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            mLocation = mLastLocation
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }
}