package com.nabeel.cameraapp.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.nabeel.cameraapp.R
import com.nabeel.cameraapp.database.ImageFileEntity
import com.nabeel.cameraapp.utils.Converters
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var imageFile: ImageFileEntity
    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        imageFile = intent.getSerializableExtra("imageDetails") as ImageFileEntity
        setData()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val location = LatLng(imageFile.lat,imageFile.lon)
        mMap.addMarker(MarkerOptions().position(location))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
        //move map camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11f))
    }

    private fun setData() {
        ic_image.setImageBitmap(Converters.getBitmapFromFilePath(imageFile.path))
        tv_fileName.text = imageFile.name
        tv_fileSize.text = imageFile.size
        tv_filePath.text = imageFile.path
        tv_fileLat.text = imageFile.lat.toString()
        tv_fileLon.text = imageFile.lon.toString()
        tv_fileTime.text = imageFile.date
    }
}