package com.nabeel.cameraapp.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nabeel.cameraapp.R
import com.nabeel.cameraapp.database.ImageFileEntity
import com.nabeel.cameraapp.utils.Converters
import kotlinx.android.synthetic.main.dialog_image_view.view.*

class ImageFileAdapter(
    var imageFileList: List<ImageFileEntity>,
    private val context: Context
) :
    RecyclerView.Adapter<ImageFileAdapter.ItemViewHolder>() {

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image = itemView.ic_image
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_photos_list, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return imageFileList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val imageFile = imageFileList[position]
        holder.image.setImageBitmap(Converters.getBitmapFromFilePath(imageFile.path))
    }

    fun setData(mImageFileList: List<ImageFileEntity>) {
        imageFileList = mImageFileList
        notifyDataSetChanged()
    }
}